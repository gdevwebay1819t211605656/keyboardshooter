import { Container , Graphics, Text, ticker } from "pixi.js";
import { create } from "domain";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";

class TitleScreen extends Container{
    constructor(){
        super();

        this.bullets = [];
        this.enemies = [];
        this.score = 0;
        this.lives = 5;

        this.interactive = true;
        this.elapsedTime = 0;
        this.ticker = ticker.shared;
        this.ticker.add(this.update, this);

        this.bgmInstance = createjs.Sound.play("assets/audio/Price.mp3",{
            loop: -1,
            volume: 1.0,
            delay: 100,
        });

        this.rect1 = new Graphics();
        this.rect1.beginFill(0x00FF00);
        this.rect1.drawRect(50,50,25,25);
        this.rect1.endFill();
        this.addChild(this.rect1);

        this.scoreText = new Text("Score: " + this.score,{
            fill : [0xFFFFFF],
            fontSize : 30,
        })
        this.scoreText.x = 25;
        this.scoreText.y = 15;
        this.addChild(this.scoreText);

        this.livesText = new Text("Lives: " + this.lives,{
            fill : [0xFFFFFF],
            fontSize : 30,
        })
        this.livesText.x = 750;
        this.livesText.y = 15;
        this.addChild(this.livesText);

        this.bulletText = new Text(" ",{
            fill : [0xFFFFFF],
            fontSize : 24,
            width : 15,
                height : 15
        });
        this.bulletText.x = 85;
        this.bulletText.y = 50;
        this.addChild(this.bulletText);

        this.bullet = new Text(this.bulletText.text,{
            fill : [0xFFFFFF],
            fontSize : 24,
            width : 15,
                height : 15
        });
        this.bullet.x = this.bulletText.x;
        this.bullet.y = this.bulletText.y;
        this.bullets.push(this.bullet);
        //this.addChild(this.bullet);

        this.enemy = new Text(" ",{
            fill : [0xFFFFFF],
            fontSize : 24,
            width : 15,
                height : 15
        });
        this.enemy.x = 950;
        this.enemy.y = Math.floor(Math.random() * (400-10 + 1) + 10);
        this.enemies.push(this.enemy);
        this.addChild(this.enemy);

        setInterval( () =>{
            console.log("Enemy");
            this.enemy = new Text(KeyValues[Math.floor(Math.random() * (90-65 + 1) + 65)],{
                fill : [0xFFFFFF],
                fontSize : 24,
                width : 15,
                height : 15
            });
            this.enemy.x = 950;
            this.enemy.y = Math.floor(Math.random() * (500-10 + 1) + 10);
            this.enemies.push(this.enemy);
            this.addChild(this.enemy);
        }, 5000);

        this.a = this.onKeyDown1.bind(this);
	    this.b = this.onKeyDown2.bind(this);
        window.addEventListener("keydown", this.a);
        window.addEventListener("keydown", this.b);
    }

    onKeyDown1(e)
    {
        if(KeyCodes.KeySpace == e.keyCode)
        {
            console.log("BANG!!!");
            this.bullet = new Text(this.bulletText.text,{
                fill : [0xFFFFFF],
                fontSize : 24,
                width : 15,
                height : 15
            });
            this.bullet.x = this.bulletText.x;
            this.bullet.y = this.bulletText.y;
            this.bullets.push(this.bullet);
            this.addChild(this.bullet);
            this.sfxInstance2 = createjs.Sound.play("assets/audio/Persona Summon Sound Effect.mp3",{
                volume: 1.0,
                delay: 100,
            });
        }
        if(e.keyCode >= 65 && e.keyCode <= 90)
        {
            this.bulletText.text = KeyValues[e.keyCode];
            this.sfxInstance1 = createjs.Sound.play("assets/audio/Persona 5 Stat Up.mp3",{
                volume: 1.0,
                delay: 100,
            });
        }
    }

    onKeyDown2(f)
    {
        if(KeyCodes.KeyArrowUp == f.keyCode)
        {
            this.rect1.y -= 10;
            this.bulletText.y -= 10;
        }
        if(KeyCodes.KeyArrowDown == f.keyCode)
        {
            this.rect1.y += 10;
            this.bulletText.y += 10;
        }
    }

    update(st)
    {
        this.elapsedTime += this.ticker.elapsedMS;
        for(var b = this.bullets.length - 1; b>=0;b--)
        {
            this.bullets[b].x += this.ticker.elapsedMS * 0.1;
        }
        for(var e = this.enemies.length - 1; e>=0;e--)
        {
            if(this.enemies[e].x <= 0)
            {
		        this.removeChild(this.enemies[e]);
                this.enemies.splice(e,1);
                this.enemies[d].destroy;
                console.log("Life Lost");
                this.lives -= 1;
                this.livesText.text = "Lives: " + this.lives;
                break;
            }
            else{
                this.enemies[e].x -= this.ticker.elapsedMS * 0.1;
            }
        }
        for(var c = this.bullets.length - 1; c>=0; c--)
        {
            var ab = this.bullets[c].getBounds();
            if(c == 0)
                {
                    return;
                }
            for(var d = this.enemies.length - 1; d>=0; d--)
            {
                if(d == 0)
                {
                    return;
                }
                var bb = this.enemies[d].getBounds();

                if(ab.x + ab.width > bb.x && ab.x < bb.x + bb.width && ab.y + ab.height > bb.y && ab.y < bb.y + bb.height)
                {
                    this.sfxInstance3 = createjs.Sound.play("assets/audio/Persona 5 Shatter.mp3",{
                        volume: 1.0,
                        delay: 100,
                });
                    if(this.bullets[c].text == this.enemies[d].text)
                    {
                        this.removeChild(this.bullets[c]);
                        this.removeChild(this.enemies[d]);
			            this.bullets[c].destroy;
			            this.enemies[d].destroy;
			            this.bullets.splice(c,1);
                        this.enemies.splice(d,1);
                        console.log("Hit");
                        this.score += 10;
                        this.scoreText.text = "Score: " + Math.floor(this.score);
			            break;
                    }
                    else if(this.bullets[c].text != this.enemies[d].text)
                    {
                        this.removeChild(this.bullets[c]);
			            this.bullets[c].destroy;
			            this.bullets.splice(c,1);
                    }
                }
            }
        }
        if(this.lives <= 0)
        {
            this.ticker.remove(this.update,this);
	        window.removeEventListener("keydown", this.a);
	        window.removeEventListener("keydown", this.b);
        }
    }
}
export default TitleScreen;