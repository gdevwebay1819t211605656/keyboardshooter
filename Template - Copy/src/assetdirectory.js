export let AssetDirectory = {
	"load": [
		"assets/images/image1.jpg",
		"assets/images/image2.jpg",
		"assets/images/image3.jpg"
	],
	"audio": [
		"assets/audio/Layer Cake.mp3",
		"assets/audio/Persona 5 Notification Tone.mp3",
		"assets/audio/Persona 5 Shatter.mp3",
		"assets/audio/Persona 5 Stat Up.mp3",
		"assets/audio/Persona Summon Sound Effect.mp3",
		"assets/audio/Price.mp3"
	]
};